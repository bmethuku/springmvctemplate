package com.geeklabs.fps.repository;

import com.geeklabs.fps.domain.UserRole;
import com.geeklabs.fps.domain.enums.UserRoles;
import com.geeklabs.fps.util.CRUD;

public interface UserRoleRepository extends CRUD<UserRole> {
	
	UserRole getUserRoleByRoleName(UserRoles roleName);
}
