package com.geeklabs.fps.repository.impl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;

import com.geeklabs.fps.domain.UserRole;
import com.geeklabs.fps.domain.enums.UserRoles;
import com.geeklabs.fps.repository.UserRoleRepository;
import com.geeklabs.fps.util.CRUDOpration;

@Repository
public class UserRoleRepositoryImpl extends CRUDOpration<UserRole> implements UserRoleRepository {
	
	@PersistenceContext
	private EntityManager entityManager;
	
	@Override
	protected EntityManager getEntityManager() {
		return entityManager;
	}
	
	@Override
	public UserRole getUserRoleByRoleName(UserRoles roleName) {
		CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<UserRole> createQuery = criteriaBuilder.createQuery(UserRole.class);
		
		Root<UserRole> root = createQuery.from(UserRole.class);
		createQuery.select(root);
		
		Predicate equalRoleName = criteriaBuilder.equal(root.get("userRole"), roleName);
		
		createQuery.where(equalRoleName);
		
		try {
			UserRole role = entityManager.createQuery(createQuery).getSingleResult();
			return role;
		} catch (Exception e) {
		}
		return null;
	}
}
