package com.geeklabs.fps.repository;

import java.util.List;

import com.geeklabs.fps.domain.User;
import com.geeklabs.fps.util.CRUD;

public interface UserRepository extends CRUD<User>{

	User getUserByEmailOrUserName(String userName, String email);
	User getUserByName(String userName);
	List<User> getUsersByUserRole(String name);
}
