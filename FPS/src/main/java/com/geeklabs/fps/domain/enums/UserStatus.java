package com.geeklabs.fps.domain.enums;

public enum UserStatus {
	NEW, ACTIVE, BANNED, BLOCKED, INACTIVE;
}
