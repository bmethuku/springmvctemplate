/*package com.geeklabs.skilldev.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.geeklabs.fps.domain.UserRole;
import com.geeklabs.fps.domain.enums.UserRoles;
import com.geeklabs.fps.service.UserRoleService;

@Service
public class UserRoleServiceImpl implements UserRoleService {

	@Autowired
	private UserRoleRepository userRoleRepository;
	
	@Override
	@Transactional(readOnly = true)
	public UserRole getUserRoleByRoleName(UserRoles roleName) {
		return userRoleRepository.getUserRoleByRoleName(roleName);
	}
}
*/