package com.geeklabs.fps.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.geeklabs.fps.domain.CustomUserDetails;
import com.geeklabs.fps.domain.User;
import com.geeklabs.fps.domain.UserRole;
import com.geeklabs.fps.domain.enums.UserRoles;
import com.geeklabs.fps.domain.enums.UserStatus;
import com.geeklabs.fps.dto.UserDto;
import com.geeklabs.fps.repository.UserRepository;
import com.geeklabs.fps.repository.UserRoleRepository;
import com.geeklabs.fps.service.UserService;
import com.geeklabs.fps.transformer.UserConverter;
import com.geeklabs.fps.util.ResponseStatus;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private UserRoleRepository userRoleRepository;

	/*@Autowired
	private EmailService emailService;*/
	
	@Override
	public UserDetails loadUserByUsername(String userNameOrEmail)
			throws UsernameNotFoundException {
		User userByEamailOrUserName = getUserByEamailOrUserName(userNameOrEmail, userNameOrEmail);
		
		if (userByEamailOrUserName != null) {
			List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
			if (UserRoles.USER == userByEamailOrUserName.getUserRole()) {
				authorities.add(new SimpleGrantedAuthority("ROLE_" + UserRoles.USER.toString()));
			} 	
			
			String password = userByEamailOrUserName.getPassword();
			String userStatus = userByEamailOrUserName.getUserStatus().toString();
			if (userStatus.equalsIgnoreCase("ACTIVE")) {
				UserDetails user = new CustomUserDetails(userByEamailOrUserName.getUserName(), password, authorities, userByEamailOrUserName.getId(),userByEamailOrUserName.getUserRole().name());
				return user;	
			}
		}
		return null;
	}
	
	@Override
	@Transactional
	public ResponseStatus deleteUser(Long id) {
		ResponseStatus responseStatus = new ResponseStatus();
		if (id != null) {
			userRepository.delete(User.class, id);
			responseStatus.setStatus("success");
			return responseStatus;
		}
		responseStatus.setStatus("error");
		return responseStatus;
	}
	
	
	
	@Override
	@Transactional
	public void setupAppUser() {
		// Create system admin
		UserDto userDto = new UserDto();
		userDto.setEmail("geeklabsapps@gmail.com");
		userDto.setFirstName("Geek");
		userDto.setLastName("Labs");
		userDto.setUserName("geek");
		userDto.setPassword("labs");
		
		User user = userRepository.getUserByEmailOrUserName(userDto.getUserName(), userDto.getEmail());
		if (user == null) {
			user  = UserConverter.convertUserDtoToUser(userDto);
//			emailService.send(userDto.getEmail(), "welcome to Skill Devlopment", "username:"+userDto.getUserName()+"    password:"+userDto.getPassword()+"");
			user.setUserRole(UserRoles.USER);
			user.setUserStatus(UserStatus.ACTIVE);
			userRepository.save(user);
		}
	}
	
	@Override
	@Transactional
	public void setupAppUserRoles() {
		//is user role exist
		UserRole userRole = userRoleRepository.getUserRoleByRoleName(UserRoles.USER);
		
		// System Admin
		if (userRole == null) {
			userRole = new UserRole();
			userRole.setUserRole(UserRoles.USER);
			userRoleRepository.save(userRole);
		}
		
		
	}

	@Transactional(readOnly = true)
	@Override
	public User getUserByEamailOrUserName(String userName, String email) {
		return userRepository.getUserByEmailOrUserName(userName, email);
	}
	
	@Override
	@Transactional(readOnly = true)
	public User getUserByName(String userName) {
		return userRepository.getUserByName(userName);
	}
	
}
