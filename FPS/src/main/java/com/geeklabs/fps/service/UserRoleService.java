package com.geeklabs.fps.service;

import com.geeklabs.fps.domain.UserRole;
import com.geeklabs.fps.domain.enums.UserRoles;

public interface UserRoleService {

	UserRole getUserRoleByRoleName(UserRoles roles);

	
}
