package com.geeklabs.fps.service;

import org.springframework.security.core.userdetails.UserDetailsService;

import com.geeklabs.fps.domain.User;
import com.geeklabs.fps.util.ResponseStatus;

public interface UserService extends UserDetailsService {

	void setupAppUserRoles();
	void setupAppUser();
	
	ResponseStatus deleteUser(Long id);
	User getUserByEamailOrUserName(String userName, String email);
	User getUserByName(String userName);
}
