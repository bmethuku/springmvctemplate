package com.geeklabs.fps.controllers;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.geeklabs.fps.service.UserService;
import com.geeklabs.fps.util.RequestMapper;

@Controller
@RequestMapping(value = "user")
public class UserController {

	@Autowired
	private UserService userService;

	@RequestMapping(value=RequestMapper.USER_LOGIN_SUCCESS, method = RequestMethod.GET)
	public String loginSuccess(Principal principal) {		
		return "home";
	}
	
	@RequestMapping(value=RequestMapper.USER_LOGIN_ERROR, method = RequestMethod.GET)
	public String loginFailure(Model map, RedirectAttributes redirectAttributes){
		redirectAttributes.addFlashAttribute("loginerrpr", true);	
		
		return "redirect:/";
	}

	@RequestMapping(value = RequestMapper.IS_ACTIVATED, method = RequestMethod.GET)
	public String isActivated(@RequestParam String activate, RedirectAttributes redirectAttributes) {
		/*ResponseStatus responseStatus = userService.isUserActivated(activate);
		if (responseStatus != null && responseStatus.getStatus().equals("success")) {
			redirectAttributes.addFlashAttribute("activationMsg", new Message("Your account is activated successfully.",Message.SUCCESS));			
		} else {
			redirectAttributes.addFlashAttribute("activationMsg", new Message("Sorry, we got error while activating your account.",Message.ERROR));
		}*/
		return "redirect:/";
	}
	
	@RequestMapping(value=RequestMapper.SETUP, method = RequestMethod.GET)
	public String setup() {	
		userService.setupAppUserRoles();
		userService.setupAppUser();
		return "login";
	}
}