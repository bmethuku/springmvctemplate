package com.geeklabs.fps.util;

import java.util.List;


public interface CRUD<T>{

	/*public void save(Object entity);
	public void delete(Object entity);*/

	<S extends T> void merge(S entity);
	
	<S extends T> void save(S entity);
	
	<S extends T> void save(Iterable<S> entities);
	
	void delete(Iterable<? extends T> entities);

	T findOne(Class<T> t, Long id);

	void delete(Class<T> t, Long id);
	
	List<T> findAll(Class<T> t);

	List<T> findAllByCreatedDate(Class<T> t);
	
//	CriteriaQuery<T> getCriteriaQuary(Class<T> t, CriteriaBuilder criteriaBuilder, String... s);
}
